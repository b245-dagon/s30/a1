db.fruits.aggregate([
		{$match: {onSale: true}},
		{$count: "fruitsOnSale"}

	]);

db.fruits.aggregate([
		{$match: {onSale: true , stock: {$gte:20}}},
		{$count: "EnoughFruits"}
	]);

db.fruits.aggregate([
		{$match: {onSale:true}},
		{$group: {_id: "$supplier_id" , avg_price : {$avg:"$price"}}}


	]);

db.fruits.aggregate([
		{$match: {onSale:true}},
		{$group: {_id: "$supplier_id" , max_price : {$max:"$price"}}}


	]);
db.fruits.aggregate([
		{$match: {onSale:true}},
		{$group: {_id: "$supplier_id" , min_price : {$min: "$price"}}},
			{$sort: {min_price:1}}


	]);